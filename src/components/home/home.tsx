import React, { Props } from 'react';
import { Redirect } from "react-router-dom";
import { APP_ROUTES } from '../../models/RoutingPaths';
import LabelRequired from '../common/LabelRequired';
import { withRouter, RouteComponentProps } from 'react-router';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { ProSidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
import 'react-pro-sidebar/dist/css/styles.css';
import images from '../../utils/images';


interface HomeState {
    buttonImg: any;
}

interface DispatchHomeProps {
}

interface HomeProps extends DispatchHomeProps, RouteComponentProps {

}

class Home extends React.Component<HomeProps, HomeState> {

    constructor(props: any) {
        super(props);

        this.state = {
            buttonImg: <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><circle cx="4" cy="4" r="4"/></svg>,
        }
    }

    componentDidMount() {

    }
    
    viewResume () {
        this.props.history.push(APP_ROUTES.HOME.toString());
    }
    redirectToSocialMedia (redirect: any) {
        if(redirect === 'git'){
            window.open('https://gitlab.com/janbaja/');
        }
        else if (redirect === 'li') {
            window.open('https://www.linkedin.com/in/janessa-baja-a610a5145/');
        } else {
            window.open('https://www.facebook.com/JanjanCBaja');
        }
        
    }
    viewSkills () {

    }

    render() {
        return ( <>
        <div>
            <div className="container-fluid m-0 p-0">
            
                <div className="row align-items-start mt-3 mb-5 ml-0 pl-0">
                    <div className="col-md-2 ml-3 pl-0">
                        <div className="mb-5 sidebar">
                            <ul className="list-group">
                                <li  onClick={() => this.viewResume()} className="list-group-item d-flex justify-content-between text-left active pointer border">
                                    Resume
                                </li>
                                <li  onClick={() => this.redirectToSocialMedia('git')} className="list-group-item d-flex justify-content-between text-left active pointer border">
                                    Personal Gitlab
                                </li>
                                <li  onClick={() => this.redirectToSocialMedia('li')} className="list-group-item d-flex justify-content-between text-left active pointer border">
                                    Linked In
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-9 mr-3">

                        <div className="main-page-container">
                            <div className="container text-left">
                                <div className="row">
                                        <div className="col p-0">
                                            <div className="container-wrapper">
                                                <div className="jumbotron container-header">
                                                    <div className="row">
                                                        <div className="col-md-3">
                                                            <div className="container-header-img">
                                                                <img src={images.profileImg}/>
                                                            </div>
                                                        </div>
                                                        <div className="col pl-0 ml-0">
                                                            <h1 className="container-header-name">Janessa Baja</h1>
                                                            <div><h4>Programmer</h4></div>
                                                            <div>An Experienced Software Developer offering 11 years in the IT industry with a focus on web
                                                                and logic development. A self-motivated developer and is passionate in developing quality and
                                                                efficient apps. Highly dependable developer and enthusiastic team player dedicated to
                                                                streamlining processes and efficiently resolving project issues.</div>
                                                            <div className="row mt-5"><div className="col">Email: baja.janessa@gmail.com</div><div className="col">Mobile: +639665662527</div></div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                                <div className="p-4 border-0 col-12">
                                                    <div><h4>Technical Expertise</h4></div>
                                                    <table className="table container-table border-0">
                                                        <tbody>
                                                            <tr><td className="border-0">Languages</td><td className="border-0">javascript, typescript, php</td></tr>
                                                            <tr><td className="border-0">Technologies</td><td className="border-0">Angular, Reactjs, HTML, CSS, SCSS/SASS, MySQL, Git, Apollo GraphQL, SVN</td></tr>
                                                        </tbody>
                                                    </table>
                                                    <div><h4>Work Experience</h4></div>
                                                    <div className="container-company mt-3">
                                                        <div><h5>Senior Software Engineer</h5></div>
                                                        <div>April 2019 - Current Company</div>
                                                        <div><h6>Qijang Technologies Sdn. Bhd. - “Malaysia”</h6></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Responsible for building and adding generic and reusable component for the system - <span className="font-weight-bold">REACTJS</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Collaborating with the team using Agile Practices - <span className="font-weight-bold">REACTJS</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Responsible in developing modules on CMS - <span className="font-weight-bold">REACTJS</span></div>
                                                    </div>
                                                    <div className="container-company mt-3">
                                                        <div><h5>Javascript Developer</h5></div>
                                                        <div>February 2018 - February 2019</div>
                                                        <div><h6>Mediabrands Global Technology Solutions Sdn. Bhd. - “Malaysia”</h6></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Revamped and optimized datalake web application using <span className="font-weight-bold">Angular 7</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed a datalake web application using <span className="font-weight-bold">Angular 5</span> and <span className="font-weight-bold">Apollo graphQL</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Collaborating regularly with the team using Agile Practices</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Responsible for building generic functions, plugins and reusable components</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Involvement with reporting of flows with the US Team</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Responsible for creation of mock-up designs and presenting it to the team</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Analyzation of requirements and used systematic approach to implement and document the task</div>
                                                    </div>
                                                    <div className="container-company mt-3">
                                                        <div><h5>Web Application Developer</h5></div>
                                                        <div>November 2016 - September 2017</div>
                                                        <div><h6>Plus65 Interactive Pte. Ltd. - “Singapore”</h6></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed Admin System using the backend <span className="font-weight-bold">PHP</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed responsive website with <span className="font-weight-bold">WORDPRESS</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Responsible for building generic functions with <span className="font-weight-bold">ANGULARJS</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Involved with client-facing and reporting of flow</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Analyze requirements and use systematic approaches to implement and document the task</div>
                                                    </div>
                                                    <div className="container-company mt-3">
                                                        <div><h5>Mid - Web Application Developer</h5></div>
                                                        <div>July 2016 - November 2016</div>
                                                        <div><h6>Promotexter - “Philippines”</h6></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed Content Management system for SMS using <span className="font-weight-bold">angular.JS,  express.js, node.js and jasmine testing</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed the backend using <span className="font-weight-bold">PHP</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Analyzed requirements and use systematic approach to implemention and documentation of the task</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Responsible for building the generic functions and plugins</div>
                                                    </div>
                                                    <div className="container-company mt-3">
                                                        <div><h5>Mid - Software Engineer</h5></div>
                                                        <div>October 2014 - July 2016</div>
                                                        <div><h6>Yondu - “Philippines”</h6></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed logic of numerous SMS promo for Globe Telecom</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed e-commerce web application with payment process using frameworks <span className="font-weight-bold">laravel and codeigniter</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Analyze requirements and use systematic approach to implement and document the task</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Responsible for building generic functions or plugins using backend <span className="font-weight-bold">PHP and YII framework</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Involved in client-facing and reporting of flows</div>
                                                    </div>
                                                    <div className="container-company mt-3">
                                                        <div><h5>Web Developer</h5></div>
                                                        <div>November 2012 - October 2014</div>
                                                        <div><h6>Gameloft Philippines, Inc. - “Philippines”</h6></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Consultant of CSS developer and junior web developers</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Coordinated with QA tester for end-to-end user testing and post-production testing</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Collaborated with the designers to create clean interfaces and simple intuitive interactions and experiences</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed facebook interfaces and tab applications that boost company marketing campaigns using <span className="font-weight-bold">HTML5, CSS3, javascript, PHP and MYSQL</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed several internal tools to automate task that were quickly adopted by other developers</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed a bug tracking tool YII and customer site revamp using framework <span className="font-weight-bold">laravel 4</span></div>
                                                    </div>
                                                    <div className="container-company mt-3">
                                                        <div><h5>Web Developer</h5></div>
                                                        <div>May 2012 - October 2012</div>
                                                        <div><h6>7<sup>th</sup> Media Design - “Philippines”</h6></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Involved in doing mock-up and design creation</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed responsive web applications using framework <span className="font-weight-bold">Laravel</span></div>
                                                    </div>
                                                    <div className="container-company mt-3">
                                                        <div><h5>Software Engineer</h5></div>
                                                        <div>January 2011 - December 2011</div>
                                                        <div><h6>Impart Solutions, Inc. - “Philippines”</h6></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Implement feature request for enhancement of products</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Resolved customer multiple issues by application of fast solutions for bug defect issues</div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Developed a compensation system using <span className="font-weight-bold">PHP and MYSQL</span></div>
                                                        <div>{this.state.buttonImg}&nbsp;&nbsp;Involved on testing, maintenance and development and payroll system, websites, ERP system, and<br/>
                                                            higher education using framworks <span className="font-weight-bold">Kohanna, Javascript framworks and libraries, html, css and complex mysql queries</span></div>
                                                    </div>
                                                    <div className="container-company mt-5">
                                                        <div><h5>Education</h5></div>
                                                        <h5>Bachelor of Science in Computer Science</h5>
                                                        <div><h6>Far Eastern University - East Asia College</h6></div>
                                                        <div>July 2006 - October 2010, Manila Philippines</div>
                                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
        </>)
    }



}



export default Home;
